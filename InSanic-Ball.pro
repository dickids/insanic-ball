#-------------------------------------------------
#
# Project created by QtCreator 2011-03-04T17:34:20
#
#-------------------------------------------------

include(../Common.pri)
TARGET = InSanic-Ball

SOURCES += \
    src/myscene.cpp \
    src/ball.cpp \
    src/controller.cpp \
    src/gamemode.cpp \
    src/gamestate.cpp \
    src/pickups/boost.cpp \
    src/playerstate.cpp \
    src/pickups/coin.cpp \
    src/pickups/pickup.cpp \
    src/levels/level.cpp \
    src/levels/level_000.cpp \
    src/levels/level_001.cpp \
    src/levels/level_002.cpp \
    src/levels/level_003.cpp \
    src/levels/level_004.cpp \
    src/levels/level_005.cpp

HEADERS += \
    src/ball.h \
    src/controller.h \
    src/gamemode.h \
    src/gamestate.h \
    src/pickups/boost.h \
    src/playerstate.h \
    src/pickups/coin.h \
    src/pickups/pickup.h \
    src/levels/level.h \
    src/levels/level_000.h \
    src/levels/level_001.h \
    src/levels/level_002.h \
    src/levels/level_003.h \
    src/levels/level_004.h \
    src/levels/level_005.h
