#include "src/ball.h"

#include "trianglemesh.h"
#include "scenemanager.h"

Ball::Ball(QVector3D fallbackVelocity): Ball(fallbackVelocity, new TriangleMesh(SRCDIR + QString("/modelstextures/ball.obj")), new Texture(SRCDIR + QString("/modelstextures/sonic_ball.png"))){

}

Ball::Ball(QVector3D fallbackVelocity, Geometry* ballObject, Texture* ballTexture): Drawable (ballObject)
{
    this->enabled = false;

    this->setProperty<Texture>(ballTexture);
    this->cameraOffset = QVector3D(0, 10, 10);
    camera = SceneManager::instance()->getActiveContext()->getCamera();
    camera->rotate(0.0f, 45.0f, 0.0f);

    this->playerState = new PlayerState();

    this->fallbackVelocity = fallbackVelocity;
}

PhysicObjectConstructionInfo* Ball::addPhysics(PhysicEngine* physicEnigne){
    PhysicObject* v_BallPhys = physicEnigne->createNewPhysicObject(this);

    PhysicObjectConstructionInfo* v_Inf = new PhysicObjectConstructionInfo();
    v_Inf->setCollisionHull(CollisionHull::SphereRadius);
    v_Inf->setMass(100000.0f);
    v_Inf->setFriction(1.0f);
    v_Inf->setRestitution(0.0f);
    v_BallPhys->setConstructionInfo(v_Inf);
    v_BallPhys->registerPhysicObject();

    return v_Inf;
}

void Ball::calcAlpha(){
    QVector3D ballVelocity = this->getPhysicObject()->getLinearVelocity();

    if(ballVelocity.x() == 0.0f && ballVelocity.z() == 0.0f) {
        ballVelocity = this->fallbackVelocity;
    }

    float tmpAlphaRad = atan(ballVelocity.x() / ballVelocity.z());
    float tmpAlphaDeg = tmpAlphaRad * 180 / PI;

    //convert angle to 360°
    if(ballVelocity.z() > 0){
        tmpAlphaDeg += 180;
    }else if(ballVelocity.x() > 0){
        tmpAlphaDeg += 360;
    }

    if (!std::isnan(tmpAlphaDeg) && !std::isnan(this->alphaDeg)){
        float tmpAlphaDiff = tmpAlphaDeg - this->alphaDeg;
        //detect jump from 360° to 0° or backwards
        if(360.0f - abs(tmpAlphaDiff) < 90){
            tmpAlphaDiff = 360.0f - abs(tmpAlphaDiff);
            if(this->alphaDeg < tmpAlphaDeg){
                tmpAlphaDiff *= -1;
            }
        }

        if(ballVelocity.length() > SHAKE_FIX_THRESHOLD) {
            if(abs(tmpAlphaDiff) > 90){
                this->backwards = !this->backwards;
            }

            this->alphaRad = tmpAlphaRad;
            this->alphaDeg = tmpAlphaDeg;
            this->alphaDiff = tmpAlphaDiff;
        }
    }
}

float Ball::getAlphaRadIncludingBackwardsFlag(){
    float alpha = this->getAlphaDeg();
    if(this->isRollingBackwards()){
        if (alpha >= 180.0f){
            alpha -= 180.0f;
        }else {
            alpha += 180.0f;
        }
    }
    return alpha;
}

void Ball::moveAndRotateCamera()
{
    //TODO: check for ball speed to remove strange glitches
    QVector3D dynamicCameraOffset;

    if(!std::isnan(this->getAlphaRad())){
        float b = cos(this->getAlphaRad()) * this->cameraOffset.z();
        float a = sin(this->getAlphaRad()) * this->cameraOffset.z();

        if(this->getAlphaRadIncludingBackwardsFlag() > 90 && this->getAlphaRadIncludingBackwardsFlag() < 270){
            b *= -1;
            a *= -1;
        }

        dynamicCameraOffset = QVector3D(a, this->cameraOffset.y(), b);

        camera->resetCCS();
        camera->rotate(-1 * this->getAlphaRadIncludingBackwardsFlag(), 0.0f, 0.0f);
        camera->rotate(0.0f, 45.0f, 0.0f);
    }else{
        dynamicCameraOffset = this->cameraOffset;
    }

    QVector3D position = this->getWorldMatrix().column(3).toVector3D() + dynamicCameraOffset;

    camera->setPosition(position);
}

void Ball::doIt(){
    if(this->enabled) {
        this->calcAlpha();

        this->moveAndRotateCamera();
    }
}
