#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "keyboardinput.h"
#include "idleobserver.h"
#include "transformation.h"
#include "node.h"
#include "ball.h"

#define ROTATION_SPEED 0.5f
#define BACKWARDS_ROTATION_SPEED 0.25f
#define MAX_ROTATION 20.0f

class Controller: public IdleObserver
{
public:
    Controller();

    virtual void doIt() override;

    Node* control(Node* n);

    void setBall(Ball* ball) {
        this->ball = ball;
    }

    void disable() { this->enabled = false; }
    void enable() { this->enabled = true; }

private:
    KeyboardInput* input;

    float rotationZ = 0.0f;
    float rotationX = 0.0f;

    Transformation* z_rotation;
    Transformation* x_rotation;

    Transformation* point_translation;
    Transformation* back_translation;

    Transformation* y_rotation;
    Transformation* y_backRotation;

    Ball* ball = nullptr;

    float oldAlpha;
    bool reverse = false;

    void rotateToRollingDirection();
    void moveToBallLocation();
    void handleKeyboardInput();

    bool enabled;
};

#endif // CONTROLLER_H
