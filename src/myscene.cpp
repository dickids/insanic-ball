#include "simplesphere.h"
#include "scenemanager.h"
#include "screenrenderer.h"
#include "camera.h"
#include "controllablecamera.h"
#include "queue"
#include "audioengine.h"

#include "src/levels/level_000.h"
#include "src/levels/level_001.h"
#include "src/levels/level_002.h"
#include "src/levels/level_003.h"
#include "src/levels/level_004.h"
#include "src/levels/level_005.h"
#include "src/gamemode.h"

void populateLevelQueue(GameMode* gm);

void SceneManager::initScenes()
{
    Camera *cam = new Camera();
    RenderingContext *myContext=new RenderingContext(cam);
    unsigned short myContextNr = SceneManager::instance()->addContext(myContext);

    unsigned short myScene = SceneManager::instance()->addScene(new Node());
    ScreenRenderer* renderer = new ScreenRenderer(myContextNr, myScene);
    Q_UNUSED(renderer);

    AudioEngine::instance().init(AudioEngineType::OpenAL3D);

    // Vorsicht: Die Szene muss initialisiert sein, bevor das Fenster verändert wird (Fullscreen)
    SceneManager::instance()->setActiveScene(myScene);

    SceneManager::instance()->setActiveContext(myContextNr);

    GameMode* gm = new GameMode(renderer);
    populateLevelQueue(gm);
    gm->startNextLevel();

    SceneManager::instance()->setFullScreen();
}

void populateLevelQueue(GameMode* gm) {
    gm->getGameState()->getLevelQueue()->push(new Level_003(gm));

    gm->getGameState()->getLevelQueue()->push(new Level_000(gm));
    gm->getGameState()->getLevelQueue()->push(new Level_001(gm));
    gm->getGameState()->getLevelQueue()->push(new Level_002(gm));
    gm->getGameState()->getLevelQueue()->push(new Level_003(gm));
    gm->getGameState()->getLevelQueue()->push(new Level_004(gm));
    gm->getGameState()->getLevelQueue()->push(new Level_005(gm));
}
