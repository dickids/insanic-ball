#ifndef PLAYERSTATE_H
#define PLAYERSTATE_H


class PlayerState
{
public:
    PlayerState();

    unsigned int getCoinCount() { return this->coinCount; }

    void increaseCoinCount() { this->coinCount++; }
    void resetCoinCount() { this->coinCount = 0; }

private:
    unsigned int coinCount;
};

#endif // PLAYERSTATE_H
