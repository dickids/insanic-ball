#include "boost.h"
#include "simplesphere.h"
#include "trianglemesh.h"
#include "modeltransformation.h"

namespace pickups {

Boost::Boost(Level* level, QVector3D location, QVector3D velocity):
    PickUp(level, new TriangleMesh(SRCDIR + QString("/modelstextures/pickups/boost.obj")), new Texture(SRCDIR + QString("/modelstextures/pickups/boost.png")), location, BOOST_RESPAWN_TIMER) {

    this->velocity = velocity;
    this->modifier = 1;

    this->getProperty<ModelTransformation>()->rotate(90.0f, 1.0f, 0.0f, 0.0f);

    this->pickUpSound = new SoundSource(new SoundFile(SRCDIR +QString("/sounds/boost.wav")));
}

Boost::Boost(Level* level, QVector3D location, int modifier):
    PickUp(level, new TriangleMesh(SRCDIR + QString("/modelstextures/pickups/boost.obj")), new Texture(SRCDIR + QString("/modelstextures/pickups/boost.png")), location, BOOST_RESPAWN_TIMER) {

    this->velocity = QVector3D(0.0f, 0.0f, 0.0f);
    this->modifier = modifier;

    this->getProperty<ModelTransformation>()->rotate(90.0f, 1.0f, 0.0f, 0.0f);

    this->pickUpSound = new SoundSource(new SoundFile(SRCDIR +QString("/sounds/boost.wav")));
}

void Boost::onPickUp(Ball* ball) {
    ball->getPhysicObject()->setLinearVelocity((ball->getPhysicObject()->getLinearVelocity() * modifier) + this->velocity);
    qDebug() << "boost velocity: " << this->velocity << " modifier: " << this->modifier;
}

}
