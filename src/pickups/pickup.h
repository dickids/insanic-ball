#ifndef PICKUP_H
#define PICKUP_H

#include "drawable.h"
#include "geometry.h"
#include "texture.h"
#include "idleobserver.h"
#include "soundsource.h"

#include "src/ball.h"
#include "src/levels/level.h"

#define DISABLE_RESPAWN -1

namespace levels {
    class Level;
}

using namespace levels;

namespace pickups {

class PickUp: public Drawable, public IdleObserver
{
public:
    PickUp(Level* level, Geometry* geometry, Texture* texture, QVector3D location);

    PickUp(Level* level, Geometry* geometry, Texture* texture, QVector3D location, int respawnTimer);

    PhysicObjectConstructionInfo* addPhysics(PhysicEngine* physicEnigne);

    void enable();
    void disable(int respawnTimer);
    void disable();

    virtual void doIt() override;

 protected:
    virtual void onPickUp(Ball* ball) = 0;

   SoundSource* pickUpSound;

private:
    void pickUpCallback(PhysicObject*& a, PhysicObject*& b, QList<CollisionPointInfo>& c);

    bool enabled;

    int respawnTimer;
    int remainingRespawnTimer;

    Level* level;
};

}

#endif // PICKUP_H
