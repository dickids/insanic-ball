#include "src/pickups/pickup.h"
#include "modeltransformation.h"
#include "shadermanager.h"

namespace pickups {

PickUp::PickUp(Level* level, Geometry* geometry, Texture* texture, QVector3D location):
    PickUp(level, geometry, texture, location, DISABLE_RESPAWN) {

}

PickUp::PickUp(Level* level, Geometry* geometry, Texture* texture, QVector3D location, int respawnTimer): Drawable (geometry), IdleObserver ()
{
    this->enabled = false;
    this->respawnTimer = respawnTimer;

    this->setStaticGeometry(true);

    this->setProperty<Texture>(texture);
    this->getProperty<ModelTransformation>()->translate(location.x(), location.y(), location.z());

    this->setShader(ShaderManager::getShader(SRCDIR + QString("/shader/texture.vert"), SRCDIR + QString("/shader/texture.frag")));

    this->level = level;
}


PhysicObjectConstructionInfo* PickUp::addPhysics(PhysicEngine* physicEnigne){
    PhysicObject* pickUpPhysics = physicEnigne->createNewPhysicObject(this);

    PhysicObjectConstructionInfo* pickUpPhysicsInfo = new PhysicObjectConstructionInfo();
    pickUpPhysicsInfo->setCollisionHull(CollisionHull::SphereRadius);
    pickUpPhysics->setConstructionInfo(pickUpPhysicsInfo);
    pickUpPhysics->setPhysicType(PhysicType::Trigger);
    pickUpPhysics->registerPhysicObject();

    SpecificResponseObject<PickUp>* callbackReceiver = new SpecificResponseObject<PickUp>(this, &PickUp::pickUpCallback);
    pickUpPhysics->addResponseObject(callbackReceiver);

    return pickUpPhysicsInfo;
}

void PickUp::pickUpCallback(PhysicObject *&a, PhysicObject *&b, QList<CollisionPointInfo> &c){
    if (this->enabled && !this->level->getGameMode()->getGameState()->arePickUpsLocked()) {
        this->pickUpSound->play();
        this->onPickUp((Ball*)b->getGeometry());
        this->disable();
    }
}

void PickUp::enable() {
    this->enabled = true;
    this->setEnabled(true);
}

void PickUp::disable() {
    this->disable(this->respawnTimer);
}

void PickUp::disable(int respawnTimer) {
    this->enabled = false;
    this->setEnabled(false);
    this->remainingRespawnTimer = respawnTimer;
}

void PickUp::doIt(){
    if(this->remainingRespawnTimer > 0){
        this->remainingRespawnTimer--;
    } else if(this->remainingRespawnTimer == 0){
        this->enable();
    }

    if(this->enabled){
        this->getProperty<ModelTransformation>()->rotate(0.1f, 0.0f, 0.0f, 1.0f);
    }
}

}
