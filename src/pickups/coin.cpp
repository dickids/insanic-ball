#include "coin.h"
#include "simplesphere.h"
#include "trianglemesh.h"
#include "modeltransformation.h"

namespace pickups {

Coin::Coin(Level* level, QVector3D location):
    PickUp (level, new TriangleMesh(SRCDIR + QString("/modelstextures/pickups/coin.obj")), new Texture(SRCDIR + QString("/modelstextures/pickups/coin.png")), location)
{
    this->getProperty<ModelTransformation>()->rotate(90.0f, 1.0f, 0.0f, 0.0f);

    this->pickUpSound = new SoundSource(new SoundFile(SRCDIR +QString("/sounds/coin.wav")));
}

void Coin::onPickUp(Ball* ball) {
    ball->getPlayerState()->increaseCoinCount();
    qDebug() << "Pick up Coin " << ball->getPlayerState()->getCoinCount();
}

}
