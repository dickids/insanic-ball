#ifndef COIN_H
#define COIN_H

#include "src/pickups/pickup.h"
#include "src/levels/level.h"

using namespace levels;

namespace pickups {

class Coin: public PickUp
{
public:
    Coin(Level* level, QVector3D location);

protected:
    void onPickUp(Ball* ball) override;
};

}

#endif // COIN_H
