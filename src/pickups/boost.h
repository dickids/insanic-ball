#ifndef BOOST_H
#define BOOST_H

#include "src/pickups/pickup.h"
#include "src/levels/level.h"

#define BOOST_RESPAWN_TIMER 600

using namespace levels;

namespace pickups {

class Boost: public PickUp
{
public:
    Boost(Level* level, QVector3D location, QVector3D velocity);

    Boost(Level* level, QVector3D location, int modifier);

protected:
    void onPickUp(Ball* ball) override;

private:
    QVector3D velocity;

    int modifier;
};

}

#endif // BOOST_H
