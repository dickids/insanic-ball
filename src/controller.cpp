#include "src/controller.h"
#include <QObject>
#include "inputregistry.h"

Controller::Controller(): IdleObserver()
{
    this->enabled = false;

    this->input = InputRegistry::getInstance().getKeyboardInput();
}

void Controller::rotateToRollingDirection()
{
    //calc angle of velocity
    float alpha = this->ball->getAlphaDeg();

    if (!std::isnan(alpha)){
        //reset the transformation
        this->y_rotation->resetTrafo();
        this->y_backRotation->resetTrafo();
        //set the new alpha
        this->y_rotation->rotate(this->ball->getAlphaRadIncludingBackwardsFlag(), 0.0f, 1.0f, 0.0f);
        this->y_backRotation->rotate(-1 * this->ball->getAlphaRadIncludingBackwardsFlag(), 0.0f, 1.0f, 0.0f);
    }
}

void Controller::moveToBallLocation()
{
    QVector3D position = this->ball->getWorldMatrix().column(3).toVector3D();

    this->point_translation->resetTrafo();
    this->back_translation->resetTrafo();

    this->point_translation->translate(position.x(), position.y(), position.z());
    this->back_translation->translate(-1 * position.x(), -1 * position.y(), -1 * position.z());
}

void Controller::handleKeyboardInput()
{
    if (this->input->isKeyPressed('d')){
        if (this->rotationZ > -MAX_ROTATION){
            this->z_rotation->rotate(-ROTATION_SPEED, 0.0f, 0.0f, 1.0f);
            this->rotationZ -= ROTATION_SPEED;
        }
    } else if (this->input->isKeyPressed('a')){
        if (this->rotationZ < MAX_ROTATION){
            this->z_rotation->rotate(ROTATION_SPEED, 0.0f, 0.0f, 1.0f);
            this->rotationZ += ROTATION_SPEED;
        }
    } else {
        if (this->rotationZ > 0.0f){
            this->z_rotation->rotate(-BACKWARDS_ROTATION_SPEED, 0.0f, 0.0f, 1.0f);
            this->rotationZ -= BACKWARDS_ROTATION_SPEED;
        } else if (this->rotationZ < 0.0f){
            this->z_rotation->rotate(BACKWARDS_ROTATION_SPEED, 0.0f, 0.0f, 1.0f);
            this->rotationZ += BACKWARDS_ROTATION_SPEED;
        }
    }

    if (this->input->isKeyPressed('w')){
        if (this->rotationX > -MAX_ROTATION){
            this->x_rotation->rotate(-ROTATION_SPEED, 1.0f, 0.0f, 0.0f);
            this->rotationX -= ROTATION_SPEED;
        }
    } else if (this->input->isKeyPressed('s')){
        if (this->rotationX < MAX_ROTATION){
            this->x_rotation->rotate(ROTATION_SPEED, 1.0f, 0.0f, 0.0f);
            this->rotationX += ROTATION_SPEED;
        }
    } else {
        if (this->rotationX > 0.0f) {
            this->x_rotation->rotate(-BACKWARDS_ROTATION_SPEED, 1.0f, 0.0f, 0.0f);
            this->rotationX -= BACKWARDS_ROTATION_SPEED;
        } else if (this->rotationX < 0.0f){
            this->x_rotation->rotate(BACKWARDS_ROTATION_SPEED, 1.0f, 0.0f, 0.0f);
            this->rotationX += BACKWARDS_ROTATION_SPEED;
        }
    }
}

void Controller::doIt(){
    if(this->ball != nullptr && this->enabled) {
        //move controller to the location of the ball
        moveToBallLocation();

        //rotate controller to the direction where the ball is rolling
        rotateToRollingDirection();
    }

    handleKeyboardInput();
}

Node* Controller::control(Node* n){
    this->point_translation = new Transformation();
    this->y_rotation = new Transformation();
    this->z_rotation = new Transformation();
    this->x_rotation = new Transformation();
    this->y_backRotation = new Transformation();
    this->back_translation = new Transformation();

    Node* point_translationNode = new Node(point_translation);
    Node* y_rotationNode = new Node(y_rotation);
    Node* z_rotationNode = new Node(z_rotation);
    Node* x_rotationNode = new Node(x_rotation);
    Node* y_backRotationNode = new Node(y_backRotation);
    Node* back_translationNode = new Node(back_translation);

    back_translationNode->addChild(n);
    y_backRotationNode->addChild(back_translationNode);
    x_rotationNode->addChild(y_backRotationNode);
    z_rotationNode->addChild(x_rotationNode);
    y_rotationNode->addChild(z_rotationNode);
    point_translationNode->addChild(y_rotationNode);

    return point_translationNode;
}
