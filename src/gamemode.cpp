#include "src/gamemode.h"
#include "scenemanager.h"

GameMode::GameMode(ScreenRenderer* renderer)
{
    this->renderer = renderer;
    this->gamestate = new GameState();
}

Ball* GameMode::getBall(QVector3D fallbackVelocity){
    return new Ball(fallbackVelocity);
}

Controller* GameMode::getController(){
    return new Controller();
}

void GameMode::startNextLevel(){
    if(this->gamestate->getLevelQueue()->size() > 0){
        if(this->gamestate->getCurrentLevel() != nullptr) {
            this->gamestate->getCurrentLevel()->stop();
        }

        this->gamestate->setCurrentLevel(this->gamestate->getLevelQueue()->front());
        this->gamestate->getLevelQueue()->pop();

        this->gamestate->getCurrentLevel()->start();

        this->renderer->setScene(this->gamestate->getCurrentLevel()->getSceneId());
    }
}

void GameMode::outOfMapCallback(){
    if (!this->gamestate->isSpawnLocked()){
        this->gamestate->getCurrentLevel()->restart();
        this->gamestate->lockSpawn();
        this->gamestate->lockPickUps();
    }
}

void GameMode::goalCallback(){
    if (!this->gamestate->isSpawnLocked()) {
        this->startNextLevel();
        this->gamestate->lockSpawn();
        this->gamestate->lockPickUps();
    }
}
