#ifndef BALL_H
#define BALL_H

#include "src/playerstate.h"

#include "drawable.h"
#include "geometry.h"
#include "texture.h"
#include "camera.h"
#include "idleobserver.h"

#define SHAKE_FIX_THRESHOLD 0.1f
#define PI 3.1415f

class Ball: public Drawable, public IdleObserver
{
public:
    Ball(QVector3D fallbackVelocity);

    Ball(QVector3D fallbackVelocity, Geometry* ballObject, Texture* ballTexture);

    PhysicObjectConstructionInfo* addPhysics(PhysicEngine* physicEnigne);

    virtual void doIt() override;

    float getAlphaDeg() {return this->alphaDeg;}
    float getAlphaRad() {return this->alphaRad;}
    float getAlphaDiff() {return this->alphaDiff;}
    bool isRollingBackwards() {return this->backwards;}
    float getAlphaRadIncludingBackwardsFlag();

    void moveAndRotateCamera();

    void disable() { this->enabled = false; }
    void enable() { this->enabled = true; }

    PlayerState* getPlayerState() { return this->playerState; }

private:
    Camera* camera;

    QVector3D cameraOffset;

    float alphaDeg;
    float alphaRad;
    float alphaDiff;
    bool backwards = true;

    void calcAlpha();

    bool enabled;

    PlayerState* playerState;

    QVector3D fallbackVelocity;
};

#endif // BALL_H
