#ifndef GAMESTATE_H
#define GAMESTATE_H

#include "idleobserver.h"
#include "src/levels/level.h"
#include "src/pickups/pickup.h"

#include "queue"

#define SPAWN_LOCK_DURATION 60
#define PICKUP_LOCK_DURATION 60

namespace levels {
    class Level;
}

namespace pickups {
    class PickUp;
}

using namespace levels;
using namespace pickups;

class GameState: public IdleObserver
{
public:
    GameState();

    virtual void doIt() override;

    bool isSpawnLocked() { return spawnLock > 0; }
    void lockSpawn() { this->spawnLock = SPAWN_LOCK_DURATION; }

    bool arePickUpsLocked() { return pickUpLock > 0; }
    void lockPickUps() { this->pickUpLock = PICKUP_LOCK_DURATION; }

    Level* getCurrentLevel() { return this->currentLevel; }
    void setCurrentLevel(Level* level) { this->currentLevel = level; }

    std::queue<Level *>* getLevelQueue() { return this->levelQueue; }

    void pickUp(PickUp* pickUp);

private:
    std::queue<Level*>* levelQueue;

    int spawnLock = SPAWN_LOCK_DURATION;

    int pickUpLock = PICKUP_LOCK_DURATION;

    Level* currentLevel = nullptr;
};

#endif // GAMESTATE_H
