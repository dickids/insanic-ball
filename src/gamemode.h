#ifndef GAMEMODE_H
#define GAMEMODE_H

#include "src/ball.h"
#include "src/controller.h"
#include "src/gamestate.h"

#include "queue"

#include "screenrenderer.h"

class GameState;

class GameMode
{
public:
    GameMode(ScreenRenderer* renderer);

    Ball* getBall(QVector3D fallbackVelocity);

    Controller* getController();

    void startNextLevel();

    void outOfMapCallback();

    void goalCallback();

    GameState* getGameState() { return this->gamestate; }
private:
    ScreenRenderer* renderer;

    GameState* gamestate;
};

#endif // GAMEMODE_H
