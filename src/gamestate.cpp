#include "gamestate.h"

GameState::GameState(): IdleObserver ()
{
    this->levelQueue = new std::queue<Level*>;
}

void GameState::doIt() {
    if(spawnLock > 0){
        spawnLock--;
    }

    if(pickUpLock > 0) {
        pickUpLock--;
    }
}
