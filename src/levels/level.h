#ifndef LEVEL_H
#define LEVEL_H

#include "node.h"
#include "physicengine.h"
#include "physics.h"
#include "shader.h"
#include "src/gamemode.h"
#include "src/pickups/pickup.h"
#include "soundsource.h"

class GameMode;

using namespace pickups;

namespace levels {

class Level: public Node
{
public:
    Level(GameMode* gameMode);

    virtual void start();

    void stop();

    void restart();

    virtual QString getTitle() = 0;

    unsigned short getSceneId() {
        return sceneId;
    }

    GameMode* getGameMode() { return this->gameMode; }
protected:
    virtual void initScene();

    PhysicEngine* physicEnigne;

    Shader* shader;

    Node* getBottomBorderPlane(float y);

    Node* getGoal(QVector3D location, float rotation);

    Node* getBall();

    Node* getController(Node* n);

    virtual Node* getMap() = 0;

    virtual QVector3D getSpawnPoint() = 0;

    virtual QVector3D getGoalLocation() = 0;

    virtual float getGoalRotation() = 0;

    void bottomBorderCollisionCallback(PhysicObject*& a, PhysicObject*& b, QList<CollisionPointInfo>& c);

    void goalCollisionCallback(PhysicObject*& a, PhysicObject*& b, QList<CollisionPointInfo>& c);

    QVector<PickUp*>* pickUpList;

    virtual QVector3D getFallbackVelocity();

private:
    GameMode* gameMode;

    Controller* controller;

    Ball* ball;

    unsigned short sceneId;

    SoundSource* ambientSound;
};

}

#endif // LEVEL_H
