#include "src/levels/level.h"
#include "simpleplane.h"
#include "drawable.h"
#include "shadermanager.h"
#include "modeltransformation.h"
#include "src/ball.h"
#include "trianglemesh.h"

#include "scenemanager.h"

namespace levels {

Level::Level(GameMode* gameMode): Node(){
    int v_Slot = PhysicEngineManager::createNewPhysicEngineSlot(PhysicEngineName::BulletPhysicsLibrary);
    this->physicEnigne = PhysicEngineManager::getPhysicEngineBySlot(v_Slot);
    this->physicEnigne->pauseSimulation();

    this->shader = ShaderManager::getShader(SRCDIR + QString("/shader/texture.vert"), SRCDIR + QString("/shader/texture.frag"));

    this->gameMode = gameMode;

    this->pickUpList = new QVector<PickUp*>();

    this->ambientSound = new SoundSource(new SoundFile(SRCDIR +QString("/sounds/background_music.wav")));
}

void Level::initScene(){
    Node* mapNode = this->getMap();
    mapNode->addChild(this->getGoal(this->getGoalLocation(), this->getGoalRotation()));

    this->addChild(this->getController(mapNode));
    this->addChild(this->getBottomBorderPlane(-50.0f));

    this->addChild(this->getBall());

    for(int i = 0; i < this->pickUpList->count(); i++){
        this->pickUpList->at(i)->addPhysics(this->physicEnigne);
        mapNode->addChild(new Node(this->pickUpList->at(i)));
    }

    this->sceneId = SceneManager::instance()->addScene(this);
}

Node* Level::getBottomBorderPlane(float y){
    Drawable* v_Plane = new Drawable(new SimplePlane(1000.f));
    v_Plane->getProperty<ModelTransformation>()->translate(0.0f, y, 0.0f);
    v_Plane->getProperty<ModelTransformation>()->rotate(90.0f, 1.f, 0.f, 0.f);
    v_Plane->setStaticGeometry(true);
    PhysicObject* v_PlanePhys = this->physicEnigne->createNewPhysicObject(v_Plane);
    PhysicObjectConstructionInfo* v_Constrinf = new PhysicObjectConstructionInfo();
    v_Constrinf->setCollisionHull(CollisionHull::BoxAABB);
    v_PlanePhys->setConstructionInfo(v_Constrinf);
    v_PlanePhys->registerPhysicObject();

    SpecificResponseObject<Level>* v_CallbackReceiver = new SpecificResponseObject<Level>(this, &Level::bottomBorderCollisionCallback);
    v_PlanePhys->addResponseObject(v_CallbackReceiver);

    return new Node(v_Plane);
}

Node* Level::getGoal(QVector3D location, float rotation){
    Texture* goal_texture = new Texture(SRCDIR + QString("/modelstextures/finish.png"));
    Drawable* goal_drawable = new Drawable(new TriangleMesh(SRCDIR + QString("/modelstextures/finish.obj")));
    goal_drawable->getProperty<ModelTransformation>()->translate(location.x(), location.y(), location.z());
    goal_drawable->getProperty<ModelTransformation>()->rotate(rotation, QVector3D(0.0f, 1.0f, 0.0f));
    goal_drawable->setProperty<Texture>(goal_texture);
    goal_drawable->setStaticGeometry(true);
    goal_drawable->setShader(this->shader);

    PhysicObject* goal_physicObj = this->physicEnigne->createNewPhysicObject(goal_drawable);
    PhysicObjectConstructionInfo* goal_PhysicConstInfo = new PhysicObjectConstructionInfo();
    goal_PhysicConstInfo->setCollisionHull(CollisionHull::BoxAABB);
    goal_physicObj->setConstructionInfo(goal_PhysicConstInfo);
    goal_physicObj->setPhysicType(PhysicType::Trigger);
    goal_physicObj->registerPhysicObject();

    SpecificResponseObject<Level>* goal_CallbackReceiver = new SpecificResponseObject<Level>(this, &Level::goalCollisionCallback);
    goal_physicObj->addResponseObject(goal_CallbackReceiver);

    return new Node(goal_drawable);
}

Node* Level::getBall(){
    this->ball = gameMode->getBall(this->getFallbackVelocity());
    QVector3D spawnPoint = this->getSpawnPoint();

    this->ball->setShader(this->shader);
    this->ball->getProperty<ModelTransformation>()->translate(spawnPoint.x(), spawnPoint.y(), spawnPoint.z());
    this->ball->addPhysics(this->physicEnigne);

    this->controller->setBall(this->ball);

    return new Node(this->ball);
}

Node* Level::getController(Node* n){
    this->controller = gameMode->getController();
    return this->controller->control(n);
}

void Level::start() {
    this->ball->enable();
    this->controller->enable();

    for(int i = 0; i < this->pickUpList->count(); i++){
        this->pickUpList->at(i)->enable();
    }

    this->physicEnigne->resumeSimulation();

    this->ambientSound->setLooping(true);
}

void Level::stop() {
   this->ball->disable();
   this->controller->disable();

    for(int i = 0; i < this->pickUpList->count(); i++){
        this->pickUpList->at(i)->disable(DISABLE_RESPAWN);
    }

    this->physicEnigne->pauseSimulation();

    this->ambientSound->pause();
}

void Level::restart() {
    QVector3D translation = QVector3D(0.0f, 0.0f, 0.0f) - this->ball->getWorldMatrix().column(3).toVector3D() + this->getSpawnPoint();
    this->ball->getProperty<ModelTransformation>()->translate(translation.x(), translation.y(), translation.z());
    this->ball->getPhysicObject()->setLinearVelocity(QVector3D(0.0f, 0.0f, 0.0f));
    this->ball->getPhysicObject()->setAngularVelocity(QVector3D(0.0f, 0.0f, 0.0f));
}

void Level::bottomBorderCollisionCallback(PhysicObject*& a, PhysicObject*& b, QList<CollisionPointInfo>& c){
    if(this->ball->getPhysicObject() == b){
        this->gameMode->outOfMapCallback();
    }
}

void Level::goalCollisionCallback(PhysicObject*& a, PhysicObject*& b, QList<CollisionPointInfo>& c){
    if(this->ball->getPhysicObject() == b){
        this->gameMode->goalCallback();
    }
}

QVector3D Level::getFallbackVelocity() {
    return this->getGoalLocation() - this->getSpawnPoint();
}

}
