#include "level_002.h"

#include "trianglemesh.h"
#include "drawable.h"
#include "modeltransformation.h"

#include "src/pickups/coin.h"

namespace levels {

Level_002::Level_002(GameMode* gameMode): Level (gameMode)
{
    this->pickUpList->append(new Coin(this, QVector3D(0.0f, 1.5f, -10.0f)));
    this->pickUpList->append(new Coin(this, QVector3D(0.0f, 1.5f, -23.0f)));
    this->pickUpList->append(new Coin(this, QVector3D(0.0f, 1.5f, -32.0f)));

    this->pickUpList->append(new Coin(this, QVector3D(0.0f, 1.5f, -63.0f)));
    this->pickUpList->append(new Coin(this, QVector3D(0.0f, 1.5f, -53.0f)));

    this->pickUpList->append(new Coin(this, QVector3D(-27.0f, 1.5f, -44.0f)));
    this->pickUpList->append(new Coin(this, QVector3D(-17.0f, 1.5f, -44.0f)));
    this->pickUpList->append(new Coin(this, QVector3D(-5.0f, 1.5f, -44.0f)));

    this->pickUpList->append(new Coin(this, QVector3D(15.0f, 1.5f, -44.0f)));
    this->pickUpList->append(new Coin(this, QVector3D(24.0f, 1.5f, -44.0f)));

    this->initScene();
}

Node* Level_002::getMap() {
    Texture* map_texture = new Texture(SRCDIR + QString("/modelstextures/levels/level_002.png"));
    Drawable* map_drawable = new Drawable(new TriangleMesh(SRCDIR + QString("/modelstextures/levels/level_002.obj")));
    map_drawable->setProperty<Texture>(map_texture);
    map_drawable->setStaticGeometry(true);
    map_drawable->setShader(this->shader);

    PhysicObject* map_physics = physicEnigne->createNewPhysicObject(map_drawable);
    PhysicObjectConstructionInfo* map_constInfo = new PhysicObjectConstructionInfo();
    map_constInfo->setCollisionHull(CollisionHull::BVHTriangleMesh);
    map_constInfo->setFriction(1.0f);
    map_physics->setConstructionInfo(map_constInfo);
    map_physics->registerPhysicObject();

    return new Node(map_drawable);
}

QVector3D Level_002::getSpawnPoint(){
    return QVector3D(0.0f, 20.0f, -3.0f);
}

QVector3D Level_002::getGoalLocation(){
    return QVector3D(0.0f, 0.0f, -44.0f);
}

float Level_002::getGoalRotation() {
    return 73.0f;
}

QString Level_002::getTitle(){
    return QString("Level 002");
}

}
