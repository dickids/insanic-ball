#ifndef LEVEL_003_H
#define LEVEL_003_H

#include "level.h"
#include "src/gamemode.h"

namespace levels {

class Level_003: public Level
{
public:
    Level_003(GameMode* gameMode);

protected:
    virtual Node* getMap() override;

    virtual QVector3D getSpawnPoint() override;

    virtual QVector3D getGoalLocation() override;

    virtual float getGoalRotation() override;

    virtual QString getTitle() override;
};

}


#endif // LEVEL_003_H
