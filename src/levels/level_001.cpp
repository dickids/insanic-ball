#include "level_001.h"

#include "trianglemesh.h"
#include "drawable.h"
#include "modeltransformation.h"

#include "src/pickups/coin.h"

namespace levels {

Level_001::Level_001(GameMode* gameMode): Level(gameMode)
{
    this->pickUpList->append(new Coin(this, QVector3D(0.0f, 1.5f, 10.0f)));
    this->pickUpList->append(new Coin(this, QVector3D(0.0f, 1.5f, 5.0f)));
    this->pickUpList->append(new Coin(this, QVector3D(5.5f, 1.5f, 4.0f)));
    this->pickUpList->append(new Coin(this, QVector3D(5.5f, 1.5f, -4.0f)));
    this->pickUpList->append(new Coin(this, QVector3D(0.0f, 1.5f, -5.0f)));
    this->pickUpList->append(new Coin(this, QVector3D(0.0f, 1.5f, -10.0f)));

    this->initScene();
}

Node* Level_001::getMap(){
    Texture* map_texture = new Texture(SRCDIR + QString("/modelstextures/levels/level_001.png"));
    Drawable* map_drawable = new Drawable(new TriangleMesh(SRCDIR + QString("/modelstextures/levels/level_001.obj")));
    map_drawable->setProperty<Texture>(map_texture);
    map_drawable->setStaticGeometry(true);
    map_drawable->setShader(this->shader);

    PhysicObject* map_physics = physicEnigne->createNewPhysicObject(map_drawable);
    PhysicObjectConstructionInfo* map_constInfo = new PhysicObjectConstructionInfo();
    map_constInfo->setCollisionHull(CollisionHull::BVHTriangleMesh);
    map_constInfo->setFriction(1.0f);
    map_physics->setConstructionInfo(map_constInfo);
    map_physics->registerPhysicObject();

    return new Node(map_drawable);
}

QVector3D Level_001::getSpawnPoint(){
    return QVector3D(0.0f, 20.0f, 14.0f);
}

QVector3D Level_001::getGoalLocation(){
    return QVector3D(0.0f, 0.0f, -12.0f);
}

float Level_001::getGoalRotation(){
    return 0.0f;
}

QString Level_001::getTitle(){
    return QString("Level 001");
}

}
