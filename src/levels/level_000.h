#ifndef LEVEL_000_H
#define LEVEL_000_H

#include "src/levels/level.h"
#include "src/pickups/pickup.h"

using namespace pickups;

namespace levels {

class Level_000: public Level
{
public:
    Level_000(GameMode* gameMode);

protected:
    virtual Node* getMap() override;

    virtual QVector3D getSpawnPoint() override;

    virtual QVector3D getGoalLocation() override;

    virtual float getGoalRotation() override;

    virtual QString getTitle() override;
};

}

#endif // LEVEL_000_H
