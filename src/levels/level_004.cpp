#include "level_004.h"

#include "trianglemesh.h"
#include "drawable.h"
#include "modeltransformation.h"

namespace levels {

Level_004::Level_004(GameMode* gameMode): Level(gameMode)
{
    this->initScene();
}

Node* Level_004::getMap() {
    Texture* map_texture = new Texture(SRCDIR + QString("/modelstextures/levels/level_004.png"));
    Drawable* map_drawable = new Drawable(new TriangleMesh(SRCDIR + QString("/modelstextures/levels/level_004.obj")));
    map_drawable->setProperty<Texture>(map_texture);
    map_drawable->setStaticGeometry(true);
    map_drawable->setShader(this->shader);

    PhysicObject* map_physics = physicEnigne->createNewPhysicObject(map_drawable);
    PhysicObjectConstructionInfo* map_constInfo = new PhysicObjectConstructionInfo();
    map_constInfo->setCollisionHull(CollisionHull::BVHTriangleMesh);
    map_constInfo->setFriction(1.0f);
    map_physics->setConstructionInfo(map_constInfo);
    map_physics->registerPhysicObject();

    return new Node(map_drawable);
}

QVector3D Level_004::getSpawnPoint(){
    return QVector3D(0.0f, 20.0f, -3.0f);
}

QVector3D Level_004::getGoalLocation(){
    return QVector3D(0.0f, 0.0f, -95.0f);
}

float Level_004::getGoalRotation() {
    return 0.0f;
}

QString Level_004::getTitle(){
    return QString("Level 004");
}

}
