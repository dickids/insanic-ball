#ifndef LEVEL_002_H
#define LEVEL_002_H

#include "level.h"
#include "src/gamemode.h"

namespace levels {

class Level_002: public Level
{
public:
    Level_002(GameMode* gameMode);

protected:
    virtual Node* getMap() override;

    virtual QVector3D getSpawnPoint() override;

    virtual QVector3D getGoalLocation() override;

    virtual float getGoalRotation() override;

    virtual QString getTitle() override;
};

}



#endif // LEVEL_002_H
