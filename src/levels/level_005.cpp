#include "level_005.h"

#include "trianglemesh.h"
#include "drawable.h"
#include "modeltransformation.h"

namespace levels {

Level_005::Level_005(GameMode* gameMode): Level(gameMode)
{
    this->initScene();
}

Node* Level_005::getMap() {
    Texture* map_texture = new Texture(SRCDIR + QString("/modelstextures/levels/level_005.png"));
    Drawable* map_drawable = new Drawable(new TriangleMesh(SRCDIR + QString("/modelstextures/levels/level_005.obj")));
    map_drawable->setProperty<Texture>(map_texture);
    map_drawable->setStaticGeometry(true);
    map_drawable->setShader(this->shader);

    PhysicObject* map_physics = physicEnigne->createNewPhysicObject(map_drawable);
    PhysicObjectConstructionInfo* map_constInfo = new PhysicObjectConstructionInfo();
    map_constInfo->setCollisionHull(CollisionHull::BVHTriangleMesh);
    map_constInfo->setFriction(1.0f);
    map_physics->setConstructionInfo(map_constInfo);
    map_physics->registerPhysicObject();

    return new Node(map_drawable);
}

QVector3D Level_005::getSpawnPoint(){
    return QVector3D(4.0f, 20.0f, 4.0f);
}

QVector3D Level_005::getGoalLocation(){
    return QVector3D(8.25f, -7.0f, 2.0f);
}

float Level_005::getGoalRotation() {
    return 0.0f;
}

QString Level_005::getTitle(){
    return QString("Level 005");
}

}
