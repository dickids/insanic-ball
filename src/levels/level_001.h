#ifndef LEVEL_001_H
#define LEVEL_001_H

#include "src/levels/level.h"

namespace levels {

class Level_001: public Level
{
public:
    Level_001(GameMode* gameMode);

protected:
    virtual Node* getMap() override;

    virtual QVector3D getSpawnPoint() override;

    virtual QVector3D getGoalLocation() override;

    virtual float getGoalRotation() override;

    virtual QString getTitle() override;
};

}

#endif // LEVEL_001_H
