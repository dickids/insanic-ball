#include "level_003.h"

#include "trianglemesh.h"
#include "drawable.h"
#include "modeltransformation.h"

#include "src/pickups/coin.h"
#include "src/pickups/boost.h"

namespace levels {

Level_003::Level_003(GameMode* gameMode): Level(gameMode)
{
    this->pickUpList->append(new Coin(this, QVector3D(-2.5f, 1.5f, -10.0f)));
    this->pickUpList->append(new Boost(this, QVector3D(-12.5f, 1.5f, -23.0f), 2));

    this->pickUpList->append(new Coin(this, QVector3D(-14.0f, 3.5f, -35.0f)));

    this->pickUpList->append(new Coin(this, QVector3D(-13.0f, 1.5f, -44.0f)));
    this->pickUpList->append(new Coin(this, QVector3D(-8.0f, 1.5f, -50.0f)));
    this->pickUpList->append(new Coin(this, QVector3D(4.0f, 1.5f, -60.0f)));

    this->initScene();
}

Node* Level_003::getMap() {
    Texture* map_texture = new Texture(SRCDIR + QString("/modelstextures/levels/level_003.png"));
    Drawable* map_drawable = new Drawable(new TriangleMesh(SRCDIR + QString("/modelstextures/levels/level_003.obj")));
    map_drawable->setProperty<Texture>(map_texture);
    map_drawable->setStaticGeometry(true);
    map_drawable->setShader(this->shader);

    PhysicObject* map_physics = physicEnigne->createNewPhysicObject(map_drawable);
    PhysicObjectConstructionInfo* map_constInfo = new PhysicObjectConstructionInfo();
    map_constInfo->setCollisionHull(CollisionHull::BVHTriangleMesh);
    map_constInfo->setFriction(1.0f);
    map_physics->setConstructionInfo(map_constInfo);
    map_physics->registerPhysicObject();

    return new Node(map_drawable);
}

QVector3D Level_003::getSpawnPoint(){
    return QVector3D(0.0f, 20.0f, -3.0f);
}

QVector3D Level_003::getGoalLocation(){
    return QVector3D(4.5f, 0.0f, -72.0f);
}

float Level_003::getGoalRotation() {
    return 0.0f;
}

QString Level_003::getTitle(){
    return QString("Level 003");
}

}


