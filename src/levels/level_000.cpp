#include "level_000.h"
#include "trianglemesh.h"
#include "drawable.h"
#include "modeltransformation.h"
#include "simplesphere.h"

#include "src/pickups/boost.h"
#include "src/pickups/coin.h"

namespace levels {

using namespace pickups;

Level_000::Level_000(GameMode* gameMode): Level(gameMode)
{
    this->pickUpList->append(new Boost(this, QVector3D(-10.0f, 1.0f, -10.0f), 10));
    this->pickUpList->append(new Coin(this, QVector3D(-10.0f, 1.0f, 10.0f)));

    this->initScene();
}

Node* Level_000::getMap(){
    Texture* map_texture = new Texture(SRCDIR + QString("/modelstextures/levels/level_000.png"));
    Drawable* map_drawable = new Drawable(new TriangleMesh(SRCDIR + QString("/modelstextures/levels/level_000.obj")));
    map_drawable->setProperty<Texture>(map_texture);
    map_drawable->setStaticGeometry(true);
    map_drawable->setShader(this->shader);

    PhysicObject* map_physics = physicEnigne->createNewPhysicObject(map_drawable);
    PhysicObjectConstructionInfo* map_constInfo = new PhysicObjectConstructionInfo();
    map_constInfo->setCollisionHull(CollisionHull::BoxAABB);
    map_constInfo->setFriction(1.0f);
    map_physics->setConstructionInfo(map_constInfo);
    map_physics->registerPhysicObject();

    Node* map_node = new Node(map_drawable);

    return map_node;
}

QVector3D Level_000::getSpawnPoint() {
    return QVector3D(0.0f, 20.0f, 0.0f);
}

QVector3D Level_000::getGoalLocation() {
    return QVector3D(15.0f, 0.0f, 15.0f);
}

float Level_000::getGoalRotation() {
    return 0.0f;
}

QString Level_000::getTitle(){
    return QString("Level 000");
}

}
