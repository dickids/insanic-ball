#ifndef LEVEL_004_H
#define LEVEL_004_H

#include "level.h"
#include "src/gamemode.h"

namespace levels {

class Level_004: public Level
{
public:
    Level_004(GameMode* gameMode);

protected:
    virtual Node* getMap() override;

    virtual QVector3D getSpawnPoint() override;

    virtual QVector3D getGoalLocation() override;

    virtual float getGoalRotation() override;

    virtual QString getTitle() override;
};

}

#endif // LEVEL_004_H
